package main

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"
	"time"
)

type Duration time.Duration

func (d Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Duration(d).String())
}

func (d *Duration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	switch value := v.(type) {
	case float64:
		*d = Duration(time.Duration(value))
		return nil
	case string:
		// Doesn't catch formats like 9d4h, but that's okay for our use case
		if strings.HasSuffix(value, "d") {
			value = strings.TrimSuffix(value, "d")
			intValue, err := strconv.Atoi(value)
			if err != nil {
				return err
			}
			*d = Duration(time.Hour * 24 * time.Duration(intValue))
			return nil
		}
		tmp, err := time.ParseDuration(value)
		if err != nil {
			return err
		}
		*d = Duration(tmp)
		return nil
	default:
		return errors.New("invalid duration")
	}
}
