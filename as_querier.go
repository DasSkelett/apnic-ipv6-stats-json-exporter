package main

import (
	"fmt"
	"net/http"
)

// ASQuerier is the API querier for per-AS requests.
// Note that the cache is shared between all ASQuerier instances.
type ASQuerier struct {
	client     *http.Client
	apiBaseURL string
}

// NewASQuerier returns a new ASQuerier, using the passed http.Client for web requests
// (can be nil, in which case it uses http.DefaultClient).
func NewASQuerier(client *http.Client) *ASQuerier {
	if client == nil {
		client = http.DefaultClient
	}
	return &ASQuerier{
		client:     client,
		apiBaseURL: APNICAPIbaseDomain,
	}
}

func (asp ASQuerier) GetAPIUrlForEntity(as string) string {
	// Example: https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=6805
	return fmt.Sprintf("%s/cgi-bin/json-table-v6.pl?x=%s", asp.apiBaseURL, as)
}

func (asp ASQuerier) HTTPGet(url string) (*http.Response, error) {
	return asp.client.Get(url)
}

func (asp ASQuerier) ConstructTimeseriesResponse(
	as string, target Target, datapoints *[][]float64) *TimeseriesResponse {
	return &TimeseriesResponse{
		Target:     fmt.Sprintf("%s_as%s", target.ToString(), as),
		Datapoints: *datapoints,
	}
}
