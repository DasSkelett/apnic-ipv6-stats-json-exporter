package main

import (
	"testing"
)

func TestCCGetAPIUrlForEntity(t *testing.T) {
	tests := []struct {
		name string
		as   string
		want string
	}{
		{
			"DE",
			"DE",
			"https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=DE",
		},
	}

	ccp := NewCountryQuerier(nil)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ccp.GetAPIUrlForEntity(tt.as); got != tt.want {
				t.Errorf("GetAPIUrlForAS() = %v, want %v", got, tt.want)
			}
		})
	}
}
