module gitlab.com/DasSkelett/apnic-ipv6-stats-json-exporter

go 1.20

require (
	github.com/goccy/go-json v0.10.2
	golang.org/x/sync v0.5.0
)
