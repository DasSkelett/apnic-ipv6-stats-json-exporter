# APNIC IPv6 stats JSON exporter for Grafana

Requires the [JSON datasource](https://grafana.com/grafana/plugins/simpod-json-datasource/) plugin to be installed!
**Tested with JSON datasource v0.6.0+ and Grafana v9.3.6.**

Fetches APNIC's per-AS IPv6 stats from `stats.labs.apnic.net`, aggregates it,
and re-exports it as JSON readable by the JSON data source plugin for Grafana.

## Links

- https://grafana.com/grafana/plugins/simpod-json-datasource/
- https://github.com/simPod/GrafanaJsonDatasource
- https://data1.labs.apnic.net/ipv6-data-format.html
- https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=6805
- https://stats.labs.apnic.net/ipv6/AS6805?a=6805&c=DE&x=0&s=0&p=1&w=7

## Installation

- Using Go: `go install gitlab.com/DasSkelett/apnic-ipv6-stats-json-exporter@latest`
- Using Docker: `docker run registry.gitlab.com/dasskelett/apnic-ipv6-stats-json-exporter:latest`
  For available tags see: <https://gitlab.com/DasSkelett/apnic-ipv6-stats-json-exporter/container_registry/1965386>

You also need simpod's [JSON datasource](https://grafana.com/grafana/plugins/simpod-json-datasource/) plugin for Grafana.
You can install it through the Grafana web UI at `/plugins`, or using the Grafana CLI:
```sh
grafana-cli plugins install simpod-json-datasource
```

## Usage

Add a new datasource to Grafana of the type `JSON`, with the URL pointing to the exporter.
The exporter listens on `:9812` by default, which can be configured with the `--listen-address` flag.

Create a new dashboard and panel, select the newly created datasource, choose a metric type from the dropdown
and enter a payload in JSON format as seen below:

```
Metric: preferred_pc
Payload: { "type": "as", "query": "6805", "smoothing": 10 }
```
or:
```
Metric: seen
Payload: { "type": "cc", "query": "DE", "smoothing": 0 }
```

Available metrics:
- "weight": the imputed weight of this data series, under an APNIC derived population/internet-users model for the world (not in all data)
- "seen": count of experiments from this source.
- "preferred": count of experiments returning on IPv6 from a dual-stack label (A/AAAA) (under happy eyeballs if implemented by the client).
- "preferred_pc": percentage of "preferred" in "seen".
- "capable": count of experiments which can fetch an IPv6 only element.
- "capable_pc": percentage of "capable" in "seen"

Available query types:
- `as`: query for a specific Autonomous System (without `AS` prefix)
- `cc`: query for a country by country code

Available smoothing:
- `0`: raw, or unsmoothed: the measurement actually made on that date; leaving out the `smoothing` property achieves the same
- `10`: measurements smoothed in a 10 day window
- `30`: measurements smoothed in a 30 day window. This approximates to 'monthly'
- `60`: measurements smoothed in a 60 day window
- `90`: measurements smoothed in a 90 day window. This approximates to 'quarterly'

## Copyright

This project is licensed under MPL-2.0, for details see the `LICENSE` file.

The original IPv6 statistics data is collected by and under copyright of the [Asia-Pacific Network Information Centre (APNIC)](https://www.apnic.net),
one of the five Regional Internet Registries (RIR).
