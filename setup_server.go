package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// This models the possible metrics and query payloads that the Grafana JSON datasource can send to us.
// This is especially useful for the Grafana query builder.

type availableMetric struct {
	Value    string                 `json:"value"`
	Label    string                 `json:"label"`
	Payloads []availablePayloadItem `json:"payloads"`
}

type availablePayloadItem struct {
	Name    string                   `json:"name"`
	Type    string                   `json:"type"`
	Options []avaylablePayloadOption `json:"options,omitempty"`
	Label   string                   `json:"label,omitempty"`
}

// Available options visible in a dropdown for a payload item when its type is "select" or "multi-select".
type avaylablePayloadOption struct {
	Value string `json:"value"`
	Label string `json:"label"`
}

var defaultPayloads = []availablePayloadItem{
	{
		Name: "type",
		Type: "select",
		Options: []avaylablePayloadOption{
			{
				Value: "as",
				Label: "Autonomous System (AS)",
			},
			{
				Value: "cc",
				Label: "Country Code (CC)",
			},
		},
	},
	{
		Name: "query",
		Type: "input",
	},
	// {
	// 	Name: "smoothing",
	// 	Type: "select",
	// 	Options: []payloadOptions{
	// 		{
	// 			Value: "0",
	// 			Label: "0 (default)",
	// 		},
	// 		{
	// 			Value: "10",
	// 			Label: "10",
	// 		},
	// 		{
	// 			Value: "30",
	// 			Label: "30",
	// 		},
	// 		{
	// 			Value: "60",
	// 			Label: "60",
	// 		},
	// 		{
	// 			Value: "90",
	// 			Label: "90",
	// 		},
	// 	},
	// },
}

var allAvailableMetricsAndPayloads = []availableMetric{
	{
		Value:    "seen",
		Label:    "seen - count of experiments from this source",
		Payloads: defaultPayloads,
	},
	{
		Value:    "preferred",
		Label:    "preferred - count of experiments returning on IPv6 from a dual-stack label (A/AAAA)",
		Payloads: defaultPayloads,
	},
	{
		Value:    "preferred_pc",
		Label:    "preferred_pc - percentage of \"preferred\" in \"seen\"",
		Payloads: defaultPayloads,
	},
	{
		Value:    "capable",
		Label:    "capable - count of experiments which can fetch an IPv6 only element",
		Payloads: defaultPayloads,
	},
	{
		Value:    "capable_pc",
		Label:    "capable_pc - percentage of \"capable\" in \"seen\"",
		Payloads: defaultPayloads,
	},
}

func setupServer(listenAddress string) *http.Server {
	// From JSON datasource docs:
	// To work with this datasource the backend needs to implement 4 endpoints:
	// - GET / with 200 status code response. Used for "Test connection" on the datasource config page.
	// - POST /metrics returning available metrics when invoked.
	// - POST /metrics returning available metrics when invoked
	//                 (for backwards-compatibility with Grafana JSON datasource <v0.6.0).
	// - POST /query returning metrics based on input.

	server := new(http.Server)
	server.Addr = listenAddress
	rh := NewRequestHandler(nil)

	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		log.Println("Received health check")
		w.WriteHeader(200)
	})

	http.HandleFunc("/search", func(w http.ResponseWriter, _ *http.Request) {
		log.Println("Received search")
		_, err := w.Write([]byte("[\"seen\",\"capable\",\"capable_pc\",\"preferred\",\"preferred_pc\"]"))
		if err != nil {
			log.Println(err)
			return
		}
	})

	http.HandleFunc("/metrics", func(w http.ResponseWriter, _ *http.Request) {
		log.Println("Received metrics search")
		err := json.NewEncoder(w).Encode(allAvailableMetricsAndPayloads)
		if err != nil {
			log.Println(err)
			return
		}
	})

	http.HandleFunc("/annotations", func(w http.ResponseWriter, _ *http.Request) {
		log.Println("Received annotations query")
		_, err := w.Write([]byte("[]"))
		if err != nil {
			log.Println(err)
			return
		}
	})

	http.HandleFunc("/query", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Received query")
		response, err := rh.HandleRequestFromReader(r.Body)
		if err != nil {
			log.Println(err)
			w.WriteHeader(400)
			_, err = w.Write([]byte(err.Error()))
			if err != nil {
				log.Println(err)
			}
			return
		}
		_, err = w.Write([]byte(response))
		if err != nil {
			log.Println(err)
			return
		}
	})
	return server
}
