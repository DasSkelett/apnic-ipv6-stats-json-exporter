image: docker:latest
services:
  - docker:dind

stages:
  - build
  - test
  - deploy

variables:
  CI_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CI_BUILD_IMAGE_BASE: $CI_REGISTRY_IMAGE/build
  CI_BUILD_IMAGE_TAG: $CI_REGISTRY_IMAGE/build:$CI_COMMIT_REF_SLUG
  DOCKER_BUILDKIT: 1
  # Prevent DinD timeout warning
  DOCKER_TLS_CERTDIR: "/certs"

docker:build:
  stage: build
  needs: []
  before_script:
    - apk add bash
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - chmod +x ./build_docker.sh && /bin/bash ./build_docker.sh
    - docker push "${CI_IMAGE_TAG}"
    - docker push "${CI_BUILD_IMAGE_TAG}"
  rules:
    - if: '$DEPENDABOT_RUN != "1"'
      changes:
      - "*.go"
      - "go.mod"
      - "Dockerfile"
      - ".gitlab-ci.yml"
      - "build_docker.sh"


docker:tests:
  stage: test
  needs:
    - docker:build
  dependencies:
    - docker:build
  services:
    - name: "$CI_IMAGE_TAG"
      alias: exporter
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add curl
  script:
    # Purposefully not hitting the metrics endpoint here, to prevent CI generating load on prod stats.labs.apnic.net
    - curl --silent --show-error "http://exporter:9812/"
  rules:
    - if: '$DEPENDABOT_RUN != "1"'
      changes:
      - "*.go"
      - "go.mod"
      - "Dockerfile"
      - ".gitlab-ci.yml"
      - "build_docker.sh"

.go-tests:
  stage: test
  needs: []
  services: []
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
    GOCACHE: $CI_PROJECT_DIR/.go-cache
  cache:
    # https://docs.gitlab.com/ee/ci/caching/index.html
    # https://docs.gitlab.com/ee/ci/caching/index.html#cache-go-dependencies
    # https://docs.gitlab.com/ee/ci/yaml/index.html#cache
    - key: go-mod-cache-$CI_JOB_NAME
      paths:
        - .go/pkg/mod/
    - key: go-build-cache-$CI_JOB_NAME
      paths:
        - .go-cache/
  before_script:
    - mkdir -p "GOPATH"; mkdir -p "$GOCACHE"
    - curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /usr/local/bin
  script:
    - go build -v ./...
    - go test -v ./...
    - go test -v -run=Benchmark -bench=Benchmark ./...
    - go test -v -run=Fuzz -fuzz=Fuzz -test.fuzztime 30s ./...
    - golangci-lint run
  rules:
    - if: '$DEPENDABOT_RUN != "1"'
      changes:
      - "*.go"
      - "go.mod"
      - ".gitlab-ci.yml"
      - ".golangci.yml"

go-tests:1.20:
  extends: .go-tests
  image: golang:1.20

go-tests:1.21:
  extends: .go-tests
  image: golang:1.21

go-tests:1.22:
  extends: .go-tests
  image: golang:1.22


docker:publish:
  stage: deploy
  # 'needs' allows to run jobs out of stage order
  needs:
    - docker:build
    - docker:tests
  # 'dependencies' defines which jobs to fetch artifacts from
  dependencies:
    - docker:build
    - docker:tests
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull "${CI_IMAGE_TAG}"

    - docker tag "${CI_IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:latest"
    - docker tag "${CI_IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:alpine"

    - docker push "${CI_REGISTRY_IMAGE}:latest"
    - docker push "${CI_REGISTRY_IMAGE}:alpine"
  # https://docs.gitlab.com/ee/ci/yaml/#rules
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'  # Publish only on default branch
      changes:                                       # 'if' and 'changes' are AND-combined
        - "*.go"
        - "go.mod"
        - "Dockerfile"
        - ".gitlab-ci.yml"
        - "build_docker.sh"
