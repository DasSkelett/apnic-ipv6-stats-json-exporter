package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"golang.org/x/sync/errgroup"
)

// RequestHandler handles requests and calls the matching parser.
type RequestHandler struct {
	client   *http.Client
	asParser *ASQuerier
	ccParser *CountryQuerier
}

func NewRequestHandler(client *http.Client) *RequestHandler {
	if client == nil {
		client = http.DefaultClient
	}
	return &RequestHandler{
		client: client,
	}
}

// TimeseriesRequest in the format of GrafanaJsonSource 0.3.0 and Grafana 8.3.0.
type TimeseriesRequest struct {
	// App         string `json:"app"`
	// RequestID   string `json:"requestId"`
	// Timezone    string `json:"timezone"`
	// PanelID     int    `json:"panelId"`
	// DashboardID int    `json:"dashboardId"`
	// TimeInfo   string `json:"timeInfo"`
	// StartTime int64 `json:"startTime"`
	Range struct {
		From time.Time `json:"from"`
		To   time.Time `json:"to"`
		Raw  struct {
			From string `json:"from"`
			To   string `json:"to"`
		} `json:"raw"`
	} `json:"range"`
	RangeRaw struct {
		From string `json:"from"`
		To   string `json:"to"`
	} `json:"rangeRaw"`
	Interval      string `json:"interval"`
	IntervalMs    int    `json:"intervalMs"`
	MaxDataPoints int    `json:"maxDataPoints"`
	ScopedVars    struct {
		Smoothing *struct {
			Text  string `json:"text"`
			Value string `json:"value"`
		} `json:"smoothing,omitempty"`
		// Interval struct {
		// 	Text  string `json:"text"`
		// 	Value string `json:"value"`
		// } `json:"__interval"`
		// IntervalMs struct {
		// 	Text  string `json:"text"`
		// 	Value int    `json:"value"`
		// } `json:"__interval_ms"`
	} `json:"scopedVars"`
	Targets []struct {
		Payload struct {
			AS        string `json:"as"`
			Type      string `json:"type"`
			Query     string `json:"query"`
			Smoothing *int   `json:"smoothing"`
		} `json:"payload,omitempty"`
		RefID      string `json:"refId"`
		Target     string `json:"target"`
		Type       string `json:"type"`
		Datasource struct {
			Type string
			UID  string
		} `json:"datasource"`
		Hide bool `json:"hide,omitempty"`
	} `json:"targets"`
	// AdhocFilters []struct {
	// 	Key      string `json:"key"`
	// 	Operator string `json:"operator"`
	// 	Value    string `json:"value"`
	// } `json:"adhocFilters"`
}

type TimeseriesResponse struct {
	Target string `json:"target"`
	// [[metric value as a float, unixtimestamp in milliseconds], ...]
	Datapoints [][]float64 `json:"datapoints"`
}

func (rh *RequestHandler) HandleRequest(request string) (string, error) {
	return rh.HandleRequestFromReader(strings.NewReader(request))
}

func (rh *RequestHandler) HandleRequestFromReader(request io.Reader) (string, error) {
	var parsedRequest TimeseriesRequest
	var responseParsed []TimeseriesResponse

	err := json.NewDecoder(request).Decode(&parsedRequest)
	if err != nil {
		return "", err
	}

	responseParsed, err = rh.HandleParsedRequest(parsedRequest)
	if err != nil {
		return "", err
	}

	response, err := json.Marshal(responseParsed)
	if err != nil {
		return "", err
	}
	return string(response), nil
}

func (rh *RequestHandler) HandleParsedRequest(request TimeseriesRequest) ([]TimeseriesResponse, error) {
	responses := make([]TimeseriesResponse, len(request.Targets))
	egroup := new(errgroup.Group)

	var dashboardSmoothing int
	var dashboardSmoothingValid bool
	var err error
	if request.ScopedVars.Smoothing != nil {
		dashboardSmoothing, err = strconv.Atoi(request.ScopedVars.Smoothing.Value)
		if err == nil {
			dashboardSmoothingValid = true
		}
	}

	for i, target := range request.Targets {
		// Copy values to avoid loop iterator problem
		i := i
		target := target

		var smoothing int
		if target.Payload.Smoothing != nil {
			smoothing = *target.Payload.Smoothing
		} else if dashboardSmoothingValid {
			smoothing = dashboardSmoothing
		} else {
			smoothing = 0
		}

		if target.Payload.Type == "as" {
			// Request for an AS -> ASParser
			if rh.asParser == nil {
				rh.asParser = NewASQuerier(rh.client)
			}

			egroup.Go(func() error {
				resp, err := AggregateStats(
					rh.asParser,
					request.Range.From,
					request.Range.To,
					target.Payload.Query,
					StringToTarget(target.Target),
					Smoothing(smoothing),
				)
				if err != nil {
					return err
				}
				responses[i] = *resp
				return nil
			})
		} else if target.Payload.Type == "cc" {
			// Request for a country code (CC) -> CountryParser
			if rh.ccParser == nil {
				rh.ccParser = NewCountryQuerier(rh.client)
			}

			egroup.Go(func() error {
				resp, err := AggregateStats(
					rh.ccParser,
					request.Range.From,
					request.Range.To,
					strings.ToUpper(target.Payload.Query),
					StringToTarget(target.Target),
					Smoothing(smoothing),
				)
				if err != nil {
					return err
				}
				responses[i] = *resp
				return nil
			})
		} else if target.Payload.AS != "" {
			// Request for an AS -> ASParser
			if rh.asParser == nil {
				rh.asParser = NewASQuerier(rh.client)
			}

			egroup.Go(func() error {
				resp, err := AggregateStats(
					rh.asParser,
					request.Range.From,
					request.Range.To,
					target.Payload.AS,
					StringToTarget(target.Target),
					Smoothing(smoothing),
				)
				if err != nil {
					return err
				}
				responses[i] = *resp
				return nil
			})
		}
	}
	return responses, egroup.Wait()
}
