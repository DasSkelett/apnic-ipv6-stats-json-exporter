package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	listenAddress = flag.String("listen-address", ":9812",
		"The address and port to listen on. To only specify a port (unspecified address), prefix it with a colon")
)

func main() {
	flag.Parse()
	server := setupServer(*listenAddress)

	errChan := make(chan error)
	sigChan := make(chan os.Signal, 2)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	fmt.Printf("Listening on %s\n", *listenAddress)
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			errChan <- err
		}
	}()

	select {
	case <-sigChan:
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		_ = server.Shutdown(ctx)
		cancel()
	case err := <-errChan:
		log.Println(err)
	}
}
