FROM golang:1.22-alpine3.19 as build

EXPOSE 9812
WORKDIR /apnic-ipv6-stats-json-exporter
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . /apnic-ipv6-stats-json-exporter
RUN go test -vet=off ./...
RUN go build -o /go/bin/apnic-ipv6-stats-json-exporter
CMD [ "apnic-ipv6-stats-json-exporter" ]

FROM alpine:3.19
COPY --from=build /go/bin/apnic-ipv6-stats-json-exporter /usr/local/bin/
EXPOSE 9812
CMD [ "apnic-ipv6-stats-json-exporter" ]
