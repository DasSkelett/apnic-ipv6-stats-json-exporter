//go:build go1.18
// +build go1.18

package main

import (
	"math"
	"testing"
)

func FuzzTarget_ToString(f *testing.F) {
	f.Add(5)
	f.Add(-99)
	f.Add(-1)
	f.Add(math.MaxInt)
	f.Add(math.MinInt)

	f.Fuzz(func(t *testing.T, i int) {
		asTarget := Target(i)
		asString := asTarget.ToString()
		fromToTarget := StringToTarget(asString)

		if int(asTarget) != i || (asString != "" && asTarget != fromToTarget) || (asString == "" && fromToTarget != -1) {
			t.Errorf("target <=> string conversion failed for %d, %v, %v, '%s'", i, asTarget, fromToTarget, asString)
		}
	})
}
