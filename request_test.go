package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func getTestServer() *httptest.Server {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		as := r.URL.Query().Get("x")
		_, err := io.Copy(w, getCachedStream(as))
		if err != nil {
			panic(err)
		}
	}))
	return server
}

const exampleQuery = `{
  "app": "dashboard",
  "requestId": "Q108",
  "timezone": "browser",
  "panelId": 2,
  "dashboardId": 16,
  "range": {
      "from": "2021-05-01T00:00:00.000Z",
      "to": "2021-05-18T00:00:00.000Z",
      "raw": {
      "from": "now-9d",
      "to": "now"
      }
  },
  "timeInfo": "",
  "interval": "24h",
  "intervalMs": 86400000,
  "targets": [
    {
      "data": "{ \"as\": \"6805\", \"smoothing\": $smoothing }",
      "payload": {
          "as": "6805",
          "smoothing": 0
      },
      "refId": "AS6805",
      "target": "preferred_pc",
      "type": "timeseries",
      "datasource": {
          "type": "simpod-json-datasource",
          "uid": "WawxBn3Gk"
	  }
    },
    {
      "data": "{ \"as\": \"3320\", \"smoothing\": $smoothing }",
      "payload": {
        "as": "3320",
        "smoothing": 0
      },
      "refId": "AS6805",
      "target": "preferred_pc",
      "type": "timeseries",
      "datasource": {
          "type": "simpod-json-datasource",
          "uid": "WawxBn3Gk"
	  }
    }
  ],
  "maxDataPoints": 10,
  "scopedVars": {
      "smoothing": {
      "text": "0",
      "value": "0"
      },
      "__interval": {
      "text": "24h",
      "value": "24h"
      },
      "__interval_ms": {
      "text": "86400000",
      "value": 86400000
      }
  },
  "startTime": 1630786901723,
  "rangeRaw": {
      "from": "now-9d",
      "to": "now"
  },
  "adhocFilters": []
}`

const testQueryASandCountry = `{
	"range": {
		"from": "2021-05-01T00:00:00.000Z",
		"to": "2021-05-18T00:00:00.000Z",
		"raw": {
		"from": "now-9d",
		"to": "now"
		}
	},
	"interval": "24h",
	"intervalMs": 86400000,
	"targets": [
	  {
		"payload": {
			"type": "as",
			"query": "6805",
			"smoothing": 0
		},
		"refId": "AS6805",
		"target": "preferred_pc",
		"type": "timeseries",
		"datasource": {
			"type": "simpod-json-datasource",
			"uid": "WawxBn3Gk"
		}
	  },
	  {
		"payload": {
			"type": "cc",
			"query": "DE",
			"smoothing": 0
		},
		"refId": "DE",
		"target": "preferred_pc",
		"type": "timeseries",
		"datasource": {
            "type": "simpod-json-datasource",
            "uid": "WawxBn3Gk"
        }
	  }
	],
	"maxDataPoints": 10,
	"scopedVars": {
		"smoothing": {
		"text": "0",
		"value": "0"
		},
		"__interval": {
		"text": "24h",
		"value": "24h"
		},
		"__interval_ms": {
		"text": "86400000",
		"value": 86400000
		}
	},
	"rangeRaw": {
		"from": "now-9d",
		"to": "now"
	}
}`

//nolint:lll
const testQueryResponse = `[{"target":"preferred_pc_as6805","datapoints":[[25.773837,1619913600000],[26.367485,1620000000000],[27.413449,1620086400000],[27.314448,1620172800000],[29.332058,1620259200000],[30.128,1620345600000],[30.621408,1620432000000],[31.336742,1620518400000],[31.327603,1620604800000],[36.24306,1620691200000],[37.611408,1620777600000],[35.850646,1620864000000],[34.614306,1620950400000],[32.374715,1621036800000],[33.271621,1621123200000],[36.536123,1621209600000]]},{"target":"preferred_pc_as3320","datapoints":[[74.183567,1619913600000],[74.278532,1620000000000],[74.207268,1620086400000],[74.832617,1620172800000],[74.926721,1620259200000],[75.759234,1620345600000],[77.532828,1620432000000],[77.006938,1620518400000],[74.683253,1620604800000],[75.574765,1620691200000],[74.50949,1620777600000],[75.711648,1620864000000],[77.274466,1620950400000],[75.627081,1621036800000],[75.197272,1621123200000],[74.305177,1621209600000]]}]`

//nolint:lll
const testQueryASandCountryResponse = `[{"target":"preferred_pc_as6805","datapoints":[[25.773837,1619913600000],[26.367485,1620000000000],[27.413449,1620086400000],[27.314448,1620172800000],[29.332058,1620259200000],[30.128,1620345600000],[30.621408,1620432000000],[31.336742,1620518400000],[31.327603,1620604800000],[36.24306,1620691200000],[37.611408,1620777600000],[35.850646,1620864000000],[34.614306,1620950400000],[32.374715,1621036800000],[33.271621,1621123200000],[36.536123,1621209600000]]},{"target":"preferred_pc_de","datapoints":[[47.56786058995412,1619913600000],[46.27865856376577,1620000000000],[46.62995244496481,1620086400000],[46.5195710089487,1620172800000],[46.44815785328694,1620259200000],[46.954552444326396,1620345600000],[46.36331558842029,1620432000000],[46.14962100406459,1620518400000],[45.26929864697039,1620604800000],[40.16840125659821,1620691200000],[38.6052133902634,1620777600000],[38.95785720815431,1620864000000],[40.73281626103236,1620950400000],[41.300646259819665,1621036800000],[42.194322186957514,1621123200000],[41.258480446133625,1621209600000]]}]`

func TestQuery(t *testing.T) {
	type args struct {
		request string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			"query0_3_0",
			args{request: exampleQuery},
			testQueryResponse,
			false,
		},
		{
			"query_as_and_country",
			args{request: testQueryASandCountry},
			testQueryASandCountryResponse,
			false,
		},
	}

	server := getTestServer()
	defer server.Close()
	rh := &RequestHandler{
		asParser: &ASQuerier{
			client:     server.Client(),
			apiBaseURL: server.URL,
		},
		ccParser: &CountryQuerier{
			client:     server.Client(),
			apiBaseURL: server.URL,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := rh.HandleRequest(tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("QueryParsed() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("QueryParsed() = %v, want %v", got, tt.want)
			}
		})
	}
}
