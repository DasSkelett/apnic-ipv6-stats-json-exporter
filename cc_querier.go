package main

import (
	"fmt"
	"net/http"
)

// CountryQuerier is the API querier for per-country requests.
// Note that the cache is shared between all CountryQuerier instances.
type CountryQuerier struct {
	client     *http.Client
	apiBaseURL string
}

// NewCountryQuerier returns a new CountryQuerier, using the passed http.Client for web requests
// (can be nil, in which case it uses http.DefaultClient).
func NewCountryQuerier(client *http.Client) *CountryQuerier {
	if client == nil {
		client = http.DefaultClient
	}
	return &CountryQuerier{
		client:     client,
		apiBaseURL: APNICAPIbaseDomain,
	}
}

func (cp CountryQuerier) GetAPIUrlForEntity(cc string) string {
	// Example: https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=DE (*)
	// Example: https://data1.labs.apnic.net/v6stats/v6economy/DE.json
	return fmt.Sprintf("%s/cgi-bin/json-table-v6.pl?x=%s", cp.apiBaseURL, cc)
}

func (cp CountryQuerier) HTTPGet(url string) (*http.Response, error) {
	return cp.client.Get(url)
}

func (cp CountryQuerier) ConstructTimeseriesResponse(
	cc string, target Target, datapoints *[][]float64) *TimeseriesResponse {
	return &TimeseriesResponse{
		Target:     fmt.Sprintf("%s_%s", target.ToString(), cc),
		Datapoints: *datapoints,
	}
}
