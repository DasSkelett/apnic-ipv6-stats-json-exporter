package main

import (
	"testing"
)

func TestASGetAPIUrlForEntity(t *testing.T) {
	tests := []struct {
		name string
		as   string
		want string
	}{
		{
			"AS3320",
			"3320",
			"https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=3320",
		},
		{
			"AS6805",
			"6805",
			"https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=6805",
		},
		{
			"DE",
			"DE",
			"https://stats.labs.apnic.net/cgi-bin/json-table-v6.pl?x=DE",
		},
	}

	asp := NewASQuerier(nil)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := asp.GetAPIUrlForEntity(tt.as); got != tt.want {
				t.Errorf("GetAPIUrlForAS() = %v, want %v", got, tt.want)
			}
		})
	}
}
