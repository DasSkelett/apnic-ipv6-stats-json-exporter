package main

import (
	"compress/gzip"
	"errors"
	"io"
	"os"
	"reflect"
	"strings"
	"sync"
	"testing"
	"time"
)

var jsonCache = make(map[string]string)
var jsonCacheLock = new(sync.Mutex)

var availableTestData = map[string][]string{
	"3320": {"AS3320_2021-09-03.json.gz"},
	"6805": {"AS6805_2021-09-05.json.gz"},
	"DE":   {"DE_2021-09-07.json.gz"},
}

func getTestData(entity string) (io.Reader, error) {
	var data []string
	var ok bool
	entity = strings.ToUpper(entity)
	if data, ok = availableTestData[entity]; !ok {
		return nil, errors.New("no testdata for AS/CC " + entity)
	}
	if len(data) < 1 {
		return nil, errors.New("no testdata for AS/CC " + entity)
	}

	file, err := os.Open("testdata/" + data[0])
	if err != nil {
		return nil, err
	}
	stream, err := gzip.NewReader(file)
	if err != nil {
		return nil, err
	}
	return stream, nil
}

func getCachedStream(entity string) io.Reader {
	jsonCacheLock.Lock()
	defer jsonCacheLock.Unlock()
	if _, ok := jsonCache[entity]; !ok {
		stream, err := getTestData(entity)
		if err != nil {
			panic(err)
		}
		bytes, err := io.ReadAll(stream)
		if err != nil {
			panic(err)
		}

		jsonCache[entity] = string(bytes)
	}
	return strings.NewReader(jsonCache[entity])
}

func benchmarkParseStats(b *testing.B, useGoJSON bool) {
	var stats *APNICJSONTable
	var err error

	for n := 0; n < b.N; n++ {
		b.StopTimer()
		stream := getCachedStream("3320")
		b.StartTimer()

		stats, err = ParseStats(useGoJSON, stream)
	}
	// Sanity checks, run only once after final iteration as not to falsify the measurement
	if err != nil {
		b.Errorf("ParseStats() error = %v", err)
		return
	}
	if stats == nil {
		b.Error("ParseStats() returned nil but no err")
		return
	}
	// if stats.Data[0].AS != "AS3320" {
	// 	b.Errorf("ParseStats() returned data for wrong AS: %s want: %s",
	// 		stats.Data[0].AS, "AS3320")
	// }
}

func BenchmarkParseStatsEncodingJSON(b *testing.B) { benchmarkParseStats(b, false) }
func BenchmarkParseStatsGoJSON(b *testing.B)       { benchmarkParseStats(b, true) }

func TestAggregateStats(t *testing.T) {
	type args struct {
		querier   Querier
		from      time.Time
		to        time.Time
		entity    string
		target    Target
		smoothing Smoothing
	}
	tests := []struct {
		name    string
		args    args
		want    *TimeseriesResponse
		wantErr bool
	}{
		{
			"from > to",
			args{from: time.Now().Add(time.Hour * 24), to: time.Now()},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := AggregateStats(tt.args.querier, tt.args.from, tt.args.to,
				tt.args.entity, tt.args.target, tt.args.smoothing)
			if (err != nil) != tt.wantErr {
				t.Errorf("AggregateStats() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AggregateStats() = %v, want %v", got, tt.want)
			}
		})
	}
}
