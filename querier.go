package main

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"
	"sync"
	"time"

	goJSON "github.com/goccy/go-json"
)

type Querier interface {
	GetAPIUrlForEntity(string) string
	HTTPGet(string) (*http.Response, error)
	ConstructTimeseriesResponse(string, Target, *[][]float64) *TimeseriesResponse
}

type Target int
type Smoothing int

const (
	// YearMonthDayFormat describes a time.Time format for the ApnicIPv6JSONTable.Data.Date field.
	YearMonthDayFormat = "2006-01-02"

	Seen Target = iota
	Capable
	CapablePc
	Preferred
	PreferredPc

	Raw         Smoothing = 0
	Smooth10Day Smoothing = 10
	Smooth30Day Smoothing = 30
	Smooth60Day Smoothing = 60
	Smooth90Day Smoothing = 90

	APNICAPIbaseDomain = "https://stats.labs.apnic.net"
)

// TargetSet is a sub-structure of APNICJSONTable
// containing the different metrics for a specific day.
// It is repeated for several smoothing factors.
type TargetSet struct {
	Seen        float64 `json:"seen"`
	Capable     float64 `json:"capable"`
	CapablePc   float64 `json:"capable_pc"`
	Preferred   float64 `json:"preferred"`
	PreferredPc float64 `json:"preferred_pc"`
}

func (m Target) ToString() string {
	switch m {
	case Seen:
		return "seen"
	case Capable:
		return "capable"
	case CapablePc:
		return "capable_pc"
	case Preferred:
		return "preferred"
	case PreferredPc:
		return "preferred_pc"
	}
	return ""
}

func StringToTarget(s string) Target {
	switch s {
	case "seen":
		return Seen
	case "capable":
		return Capable
	case "capable_pc":
		return CapablePc
	case "preferred":
		return Preferred
	case "preferred_pc":
		return PreferredPc
	}
	return -1
}

func GetTargetValueFromTargetSet(set TargetSet, target Target) float64 {
	var value float64
	switch target {
	case Seen:
		value = set.Seen
	case Capable:
		value = set.Capable
	case CapablePc:
		value = set.CapablePc
	case Preferred:
		value = set.Preferred
	case PreferredPc:
		value = set.PreferredPc
	}
	return value
}

// APNICJSONTable represents the structure of per-AS and per-country IPv6 usage data
// as returned by the API.
type APNICJSONTable struct {
	// Copyright   string `json:"copyright"`
	// Description string `json:"description"`
	// Docs        string `json:"docs"`
	Data []struct {
		Raw         TargetSet `json:"raw"`
		Smooth10Day TargetSet `json:"10"`
		Smooth30Day TargetSet `json:"30"`
		Smooth60Day TargetSet `json:"60"`
		Smooth90Day TargetSet `json:"90"`
		Type        string    `json:"type"`
		Date        string    `json:"date"` // 2011-08-30 can't be unmarshalled to time.Time
		// AS          string    `json:"as"`
		// CC          string    `json:"cc"`
		Updated string `json:"updated"`
	} `json:"data"`
	Fetched time.Time `json:"-"`
}

// tableCache caches the returned ApnicIPv6JSONTable per AS
// so it doesn't need to be re-downloaded and re-parsed with every request from Grafana.
var tableCache = map[string]*APNICJSONTable{}
var tableCacheLock sync.Mutex

// Lock cacheLock before accessing tableLocks!
var tableCacheOnce = map[string]*sync.Once{}

// FetchStats downloads and parses the statistic data for a certain AS or country entity,
// or returns it from cache if it is <2h old.
func FetchStats(parser Querier, entity string, useGoJSON bool) (APNICJSONTable, error) {
	// Check tableCache for a cached table for this AS – must be synced by locking tableCacheLock
	tableCacheLock.Lock()
	if cached := tableCache[entity]; cached != nil {
		if time.Since(cached.Fetched) < time.Hour*2 {
			tableCacheLock.Unlock()
			return *cached, nil
		}
		// Else clear the sync.Once already-done flag so the data can be refreshed
		tableCacheOnce[entity] = new(sync.Once)
	} else {
		// Not cached yet, create new sync.Once
		// but don't overwrite an existing one, a run might be in progress right now
		if tableCacheOnce[entity] == nil {
			tableCacheOnce[entity] = new(sync.Once)
		}
	}
	once := tableCacheOnce[entity]
	tableCacheLock.Unlock()

	// While we were awaiting the lock, another goroutine might have already
	// fetched and stored the data for this entity into the lock.
	// once.Do() will thus be skipped, and we read the cache later.

	once.Do(
		func() {
			jsonStream, err := DownloadStats(parser, entity)
			if err != nil {
				return
			}

			respParsed, err := ParseStats(useGoJSON, jsonStream)
			if err != nil {
				return
			}
			respParsed.Fetched = time.Now()
			// Write it into the cache, again synced using tableCacheLock
			tableCacheLock.Lock()
			tableCache[entity] = respParsed
			tableCacheLock.Unlock()
		})

	tableCacheLock.Lock()
	defer tableCacheLock.Unlock()
	if jsonTable := tableCache[entity]; jsonTable != nil {
		return *jsonTable, nil
	}
	return APNICJSONTable{}, errors.New("something went wrong while downloading or parsing the APNIC data")
}

func DownloadStats(parser Querier, entity string) (io.Reader, error) {
	resp, err := parser.HTTPGet(parser.GetAPIUrlForEntity(entity))
	if err != nil {
		return nil, err
	}
	return resp.Body, err
}

func ParseStats(useGoJSON bool, jsonStream io.Reader) (respParsed *APNICJSONTable, err error) {
	respParsed = new(APNICJSONTable)
	if useGoJSON {
		err = goJSON.NewDecoder(jsonStream).Decode(respParsed)
	} else {
		err = json.NewDecoder(jsonStream).Decode(respParsed)
	}
	return respParsed, err
}

func AggregateStats(querier Querier, from, to time.Time, entity string,
	target Target, smoothing Smoothing) (*TimeseriesResponse, error) {
	if !from.Before(to) {
		return nil, errors.New("from date can not be before to")
	}
	if target < 0 {
		return nil, errors.New("invalid target")
	}
	entity = strings.TrimPrefix(strings.ToLower(entity), "as")

	// Create a slice of (value, time) "tuples".
	days := int(to.Sub(from).Hours() / 24)
	datapoints := make([][]float64, 0, days)

	stats, err := FetchStats(querier, entity, false)
	if err != nil {
		return nil, err
	}

	for _, dayData := range stats.Data {
		date, err := time.Parse(YearMonthDayFormat, dayData.Date)
		if err != nil {
			continue
		}
		if date.After(from) && date.Before(to) {
			var value float64
			switch smoothing {
			case Raw:
				value = GetTargetValueFromTargetSet(dayData.Raw, target)
			case Smooth10Day:
				value = GetTargetValueFromTargetSet(dayData.Smooth10Day, target)
			case Smooth30Day:
				value = GetTargetValueFromTargetSet(dayData.Smooth30Day, target)
			case Smooth60Day:
				value = GetTargetValueFromTargetSet(dayData.Smooth60Day, target)
			case Smooth90Day:
				value = GetTargetValueFromTargetSet(dayData.Smooth90Day, target)
			}

			datapoints = append(datapoints, []float64{value, float64(date.Unix() * 1000)})
		}
	}

	response := querier.ConstructTimeseriesResponse(entity, target, &datapoints)
	return response, nil
}
